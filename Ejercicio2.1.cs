using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio
{
    public class Contacto
    {
        private string nombre;
        private string apellidos;
        private string telefono;
        private string direccion;
        
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        public string Telefono{ get; set; }

        public string Direccion { get; set; }
    }
}

